/* SPDX-License-Identifier: GPL-3.0-or-later */

/*
Copyright (c) 2013 - 2017 Jason Lee @ calccrypto at gmail.com
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
 * FIPS 180-2 SHA-224/256/384/512 implementation
 * Last update: 02/02/2007
 * Issue date:  04/30/2005
 *
 * Copyright (C) 2005, 2007 Olivier Gay <olivier.gay@a3.epfl.ch>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "ipfs-tiny/crypto/sha256.hpp"

namespace ipfs_tiny::crypto
{

template <typename T>
const uint32_t sha256<T>::K[s_block_size] = {
    0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1,
    0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
    0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786,
    0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
    0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
    0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
    0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b,
    0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
    0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a,
    0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
    0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};

template <>
etl::ivector<uint8_t> &
sha256<etl::ivector<uint8_t>>::digest(etl::ivector<uint8_t> &res)
{
  finalize();
  for (size_t i = 0; i < s_block_size / 8; i++) {
    res.push_back(m_h[i] >> 24);
    res.push_back(m_h[i] >> 16);
    res.push_back(m_h[i] >> 8);
    res.push_back(m_h[i]);
  }
  return res;
}

template <>
void
sha256<etl::ivector<uint8_t>>::update(const etl::ivector<uint8_t> &data)
{
  size_t tmp_len = s_block_size - m_block.size();

  size_t remaining = std::min(data.size(), tmp_len);
  m_block.insert(m_block.end(), data.begin(), data.begin() + remaining);

  if (m_block.size() < s_block_size) {
    return;
  }

  calc(m_block.begin(), m_block.end());
  m_block.clear();
  const size_t data_remaining = data.size() - remaining;
  etl::ivector<uint8_t>::const_iterator it_end_full_block =
      data.begin() + remaining + (data_remaining / s_block_size) * s_block_size;

  calc(data.begin() + remaining, it_end_full_block);
  std::copy(it_end_full_block, data.end(), m_block.begin());
}

template <>
void
sha256<etl::ivector<uint8_t>>::update(
    etl::ivector<uint8_t>::const_iterator begin,
    etl::ivector<uint8_t>::const_iterator end)
{
  size_t tmp_len = s_block_size - m_block.size();

  size_t remaining = std::min<size_t>(end - begin, tmp_len);
  m_block.insert(m_block.end(), begin, begin + remaining);

  if (m_block.size() < s_block_size) {
    return;
  }

  calc(m_block.begin(), m_block.end());
  m_block.clear();
  const size_t data_remaining = (end - begin) - remaining;
  etl::ivector<uint8_t>::const_iterator it_end_full_block =
      begin + remaining + (data_remaining / s_block_size) * s_block_size;

  calc(begin + remaining, it_end_full_block);
  std::copy(it_end_full_block, end, m_block.begin());
}

template <>
etl::ivector<uint8_t> &
sha256<etl::ivector<uint8_t>>::digest(etl::ivector<uint8_t> &      res,
                                      const etl::ivector<uint8_t> &data)
{
  reset();
  update(data);
  finalize();
  return digest(res);
}

template <>
etl::ivector<uint8_t> &
sha256<etl::ivector<uint8_t>>::digest(
    etl::ivector<uint8_t> &res, etl::ivector<uint8_t>::const_iterator begin,
    etl::ivector<uint8_t>::const_iterator end)
{
  reset();
  update(begin, end);
  finalize();
  return digest(res);
}

} // namespace ipfs_tiny::crypto
