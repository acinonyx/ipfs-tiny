/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "ipfs-tiny/exception.hpp"
#include "etl/string_stream.h"

namespace ipfs_tiny
{

#if IPFS_EXCEPT_VERBOSE == 1
exception::exception(const char *reason, const char *file, size_t line)
    : m_reason(reason), m_verbose(true), m_file(file), m_line(line)
{
}
#else
exception::exception(const char *reason, const char *file, size_t line)
    : m_reason(reason)
{
}
#endif

#if IPFS_EXCEPT_VERBOSE == 1
exception::exception(const char *reason)
    : m_reason(reason), m_verbose(false), m_file(0), m_line(0)
{
}
#else
exception::exception(const char *reason) : m_reason(reason) {}
#endif
const char *
exception::what() noexcept
{
#if IPFS_EXCEPT_VERBOSE == 1
  if (m_verbose) {
    m_str.clear();
    etl::string_stream stream(m_str);
    stream << m_file << ":" << m_line << ": " << m_reason;
    return m_str.c_str();
  }
  return m_reason;

#else
  return m_reason;
#endif
}

} // namespace ipfs_tiny
