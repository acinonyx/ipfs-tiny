/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef TEST_X86_TEST_MULTIBASE_HPP_
#define TEST_X86_TEST_MULTIBASE_HPP_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class test_multibase : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_multibase);
  CPPUNIT_TEST(test_b16);
  CPPUNIT_TEST(test_b58);
  CPPUNIT_TEST(test_unknown);
  CPPUNIT_TEST_SUITE_END();

public:
  void
  test_b16();

  void
  test_b58();

  void
  test_unknown();
};

#endif /* TEST_X86_TEST_MULTIBASE_HPP_ */
