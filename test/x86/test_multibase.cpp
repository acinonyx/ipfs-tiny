/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "test_multibase.hpp"
#include "ipfs-tiny/multiformats/multibase.hpp"
#include <random>

using namespace ipfs_tiny::multiformats;

void
test_multibase::test_b16()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t                  msg_len = 1024;
  etl::vector<uint8_t, msg_len> v0;
  etl::vector<uint8_t, msg_len> v1;
  etl::string<msg_len * 2 + 1>  res;

  for (size_t i = 0; i < msg_len; i++) {
    v0.push_back(uni(mt));
  }

  multibase::encode<multibase::encoding::base16, etl::istring,
                    etl::ivector<uint8_t>>(res, v0);

  multibase::decode<etl::ivector<uint8_t>, etl::istring>(v1, res.cbegin(),
                                                         res.cend());
  CPPUNIT_ASSERT(v0 == v1);

  v1.clear();
  multibase::decode<multibase::encoding::extract, etl::ivector<uint8_t>,
                    etl::istring>(v1, res.cbegin(), res.cend());
  CPPUNIT_ASSERT(v0 == v1);

  /* Explicitly decode using the known base */
  v1.clear();
  multibase::decode<multibase::encoding::base16, etl::ivector<uint8_t>,
                    etl::istring>(v1, res.cbegin() + 1, res.cend());
  CPPUNIT_ASSERT(etl::equal(v0.cbegin(), v0.cend(), v1.cbegin()));

  /* Check if the base is identified correctly with all the available ways */
  CPPUNIT_ASSERT(multibase::encoding::base16 ==
                 multibase::get_base<etl::istring>(res.cbegin()));
  v1.clear();
  multibase::encoding enc = multibase::encoding::extract;
  multibase::decode<etl::ivector<uint8_t>, etl::istring>(v1, enc, res.cbegin(),
                                                         res.cend());
  CPPUNIT_ASSERT(multibase::encoding::base16 == enc);
}

void
test_multibase::test_b58()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t                  msg_len = 1024;
  etl::vector<uint8_t, msg_len> v0;
  etl::vector<uint8_t, msg_len> v1;
  etl::string<msg_len * 2 + 1>  res;

  for (size_t i = 0; i < msg_len; i++) {
    v0.push_back(uni(mt));
  }
  multibase::encode<multibase::encoding::base58btc, etl::istring,
                    etl::ivector<uint8_t>>(res, v0);
  multibase::decode<etl::ivector<uint8_t>, etl::istring>(v1, res.cbegin(),
                                                         res.cend());
  CPPUNIT_ASSERT(v0 == v1);

  v1.clear();
  multibase::decode<multibase::encoding::extract, etl::ivector<uint8_t>,
                    etl::istring>(v1, res.cbegin(), res.cend());
  CPPUNIT_ASSERT(v0 == v1);

  /* Explicitly decode using the known base */
  v1.clear();
  multibase::decode<multibase::encoding::base58btc, etl::ivector<uint8_t>,
                    etl::istring>(v1, res.cbegin() + 1, res.cend());
  CPPUNIT_ASSERT(etl::equal(v0.cbegin(), v0.cend(), v1.cbegin()));

  /* Check if the base is identified correctly with all the available ways */
  CPPUNIT_ASSERT(multibase::encoding::base58btc ==
                 multibase::get_base<etl::istring>(res.cbegin()));
  v1.clear();
  multibase::encoding enc = multibase::encoding::extract;
  multibase::decode<etl::ivector<uint8_t>, etl::istring>(v1, enc, res.cbegin(),
                                                         res.cend());
  CPPUNIT_ASSERT(multibase::encoding::base58btc == enc);
}

void
test_multibase::test_unknown()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t                  msg_len = 1024;
  etl::vector<uint8_t, msg_len> v0;
  etl::vector<uint8_t, msg_len> v1;
  etl::string<msg_len * 2 + 1>  res;

  for (size_t i = 0; i < msg_len; i++) {
    v0.push_back(uni(mt));
  }
  multibase::encode<multibase::encoding::base58btc, etl::istring,
                    etl::ivector<uint8_t>>(res, v0);
  /* Explicitly change the base with something invalid */
  res[0] = 0xff;
  CPPUNIT_ASSERT_THROW((multibase::decode<etl::ivector<uint8_t>, etl::istring>(
                           v1, res.cbegin(), res.cend())),
                       multibase::uknown_base);
}
