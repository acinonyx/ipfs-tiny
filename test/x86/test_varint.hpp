/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef TEST_X86_TEST_VARINT_HPP_
#define TEST_X86_TEST_VARINT_HPP_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class test_varint : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_varint);
  CPPUNIT_TEST(random_63);
  CPPUNIT_TEST(random_47);
  CPPUNIT_TEST(random_31);
  CPPUNIT_TEST(random_23);
  CPPUNIT_TEST(random_15);
  CPPUNIT_TEST(random_7);
  CPPUNIT_TEST(back_to_back);
  CPPUNIT_TEST(invalid);
  CPPUNIT_TEST_SUITE_END();

public:
  void
  random_63();

  void
  random_47();

  void
  random_31();

  void
  random_23();

  void
  random_15();

  void
  random_7();

  void
  back_to_back();

  void
  invalid();
};

#endif /* TEST_X86_TEST_VARINT_HPP_ */
