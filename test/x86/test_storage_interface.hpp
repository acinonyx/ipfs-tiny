/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef TEST_X86_TEST_STORAGE_INTERFACE_HPP_
#define TEST_X86_TEST_STORAGE_INTERFACE_HPP_

#include "ipfs-tiny/interfaces/storage.hpp"
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <etl/string.h>
#include <etl/vector.h>
#include <iostream>
#include <map>

class dummyStorage : public ipfs_tiny::storage
{
public:
  bool
  store(const etl::istring &name, const etl::ivector<uint8_t> &data)
  {
    int                  len = data.size();
    std::vector<uint8_t> temp(&data[0], &data[0] + len);
    mem_map.insert(std::make_pair(name.c_str(), temp));
    return true;
  };

  bool
  read(const etl::istring &name, etl::ivector<uint8_t> &res)
  {
    if (mem_map.find(name.c_str()) == mem_map.end()) {
      return false;
    }
    std::vector temp = mem_map.at(name.c_str());
    res.insert(res.end(), temp.begin(), temp.end());
    for (auto i : res) {
      std::cout << (int)i;
    }
    return true;
  };

  bool
  erase(const etl::istring &name)
  {
    mem_map.erase(name.c_str());
    return (mem_map.find(name.c_str()) == mem_map.end());
  }

  size_t
  available_space()
  {
    return 0;
  }

  size_t
  garbage_collection()
  {
    return 0;
  }

private:
  std::map<std::string, std::vector<uint8_t>> mem_map;
};

class test_storage_interface : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_storage_interface);
  CPPUNIT_TEST(test_basic);
  CPPUNIT_TEST_SUITE_END();

public:
  void
  test_basic();
};

#endif /* TEST_X86_TEST_STORAGE_INTERFACE_HPP_ */
