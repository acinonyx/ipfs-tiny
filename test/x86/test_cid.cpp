/* SPDX-License-Identifier: GPL-3.0-or-later */ #include <random>

#include "test_cid.h"
#include "ipfs-tiny/cid.hpp"
#include "ipfs-tiny/crypto/sha256.hpp"

    using namespace ipfs_tiny;
using namespace ipfs_tiny::multiformats::multibase;
using namespace ipfs_tiny::multiformats;

void
string_to_vector_uint8_(etl::istring &input, etl::ivector<uint8_t> &output)
{
  for (etl::istring::iterator it = input.begin(); it != input.end(); ++it)
    output.push_back((uint8_t)*it);
}

void
test_cid::basic_encoding()
{
  etl::vector<uint8_t, 50>  msg;
  ipfs_tiny::crypto::sha256 h0 =
      ipfs_tiny::crypto::sha256<etl::ivector<uint8_t>>();
  cid x;
  x.encode<encoding::base58btc, cid_version::v1, multicodec::dag_pb>(h0, msg);
}

void
test_cid::basic_decoding_str()
{
  etl::string<50> cidv0 =
      etl::make_string("QmY7Yh4UquoXHLPFo2XbhXkhBvFoPwmQUSa92pxnxjQuPU");
  etl::string<50> cidv1_58 =
      etl::make_string("zdj7WfCo4VYhPH8A3hBXmVDZubFp8TF7VBYLkyfhdMTnAyoZQ");
  etl::string<100> cidv1_16 =
      etl::make_string("f017012209139839e65fabea9efd230898ad8b574509147e48d7c1e"
                       "87a33d6da70fd2efbf");
  cid      x;
  cid<256> y;
  cid      z;
  cid      k;
  x.decode<etl::istring>(cidv0.cbegin(), cidv0.cend());
  y.decode(cidv0);
  z.decode(cidv1_58);
  k.decode(cidv1_16);

  CPPUNIT_ASSERT(x == y);

  CPPUNIT_ASSERT(x != z);
  CPPUNIT_ASSERT(x.version() != z.version());
  CPPUNIT_ASSERT(x.base() == z.base());
  CPPUNIT_ASSERT(x.codec() == z.codec());
  CPPUNIT_ASSERT(x.multihash_buffer() == z.multihash_buffer());

  CPPUNIT_ASSERT(x != k);
  CPPUNIT_ASSERT(x.version() != k.version());
  CPPUNIT_ASSERT(x.base() != k.base());
  CPPUNIT_ASSERT(x.codec() == k.codec());
  CPPUNIT_ASSERT(x.multihash_buffer() == k.multihash_buffer());
}

void
test_cid::basic_decoding_vec()
{
  etl::string<50> cidv0s =
      etl::make_string("QmY7Yh4UquoXHLPFo2XbhXkhBvFoPwmQUSa92pxnxjQuPU");
  etl::vector<uint8_t, 50> cidv0;
  string_to_vector_uint8_(cidv0s, cidv0);

  etl::string<50> cidv1_58s =
      etl::make_string("zdj7WfCo4VYhPH8A3hBXmVDZubFp8TF7VBYLkyfhdMTnAyoZQ");
  etl::vector<uint8_t, 50> cidv1_58;
  string_to_vector_uint8_(cidv1_58s, cidv1_58);

  etl::string<100> cidv1_16s =
      etl::make_string("f017012209139839e65fabea9efd230898ad8b574509147e48d7c1e"
                       "87a33d6da70fd2efbf");
  etl::vector<uint8_t, 100> cidv1_16;
  string_to_vector_uint8_(cidv1_16s, cidv1_16);
  cid      x;
  cid<256> y;
  cid      z;
  cid      k;
  x.decode(cidv0);
  y.decode(cidv0);
  z.decode(cidv1_58);
  k.decode(cidv1_16);

  CPPUNIT_ASSERT(x == y);

  CPPUNIT_ASSERT(x != z);
  CPPUNIT_ASSERT(x.version() != z.version());
  CPPUNIT_ASSERT(x.base() == z.base());
  CPPUNIT_ASSERT(x.codec() == z.codec());
  CPPUNIT_ASSERT(x.multihash_buffer() == z.multihash_buffer());

  CPPUNIT_ASSERT(x != k);
  CPPUNIT_ASSERT(x.version() != k.version());
  CPPUNIT_ASSERT(x.base() != k.base());
  CPPUNIT_ASSERT(x.codec() == k.codec());
  CPPUNIT_ASSERT(x.multihash_buffer() == k.multihash_buffer());
}

void
test_cid::to_string()
{
  etl::vector<uint8_t, 64> vec;
  for (int i = 0; i < 64; i++) {
    vec.push_back(i);
  }

  ipfs_tiny::crypto::sha256 h0 =
      ipfs_tiny::crypto::sha256<etl::ivector<uint8_t>>();

  cid x;
  x.encode<multiformats::multibase::encoding::base58btc, cid_version::v0,
           multiformats::multicodec::code::dag_pb>(h0, vec);

  etl::string<256> output;
  x.to_string(output);

  CPPUNIT_ASSERT(output == "QmfRqT1juLUuibwaB5PGWAnGAgJz8K5m8rPvGqV6g2WSnT");

  cid y;
  y.encode<multiformats::multibase::encoding::base58btc, cid_version::v1,
           multiformats::multicodec::code::dag_pb>(h0, vec);
  output.clear();
  y.to_string(output);

  CPPUNIT_ASSERT(output == "zdj7WnX5pSokoxWbKFVupLtNYcpntWRqrVu1AoSwdsmVU5LxP");

  cid z;
  z.encode<multiformats::multibase::encoding::base16, cid_version::v1,
           multiformats::multicodec::code::dag_pb>(h0, vec);
  output.clear();
  z.to_string(output);

  CPPUNIT_ASSERT(output == "f01701220fdeab9acf3710362bd2658cdc9a29e8f9c757fcf98"
                           "11603a8c447cd1d9151108");
}

void
test_cid::full_encoding()
{
}
