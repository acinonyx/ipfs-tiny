/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "test_base16.hpp"
#include "ipfs-tiny/multiformats/base16.hpp"
#include <random>

void
test_base16::t0()
{
  ipfs_tiny::multiformats::base16<etl::ivector<uint8_t>, etl::istring> b;
  CPPUNIT_ASSERT(b.decode_size(16) == 8);
  CPPUNIT_ASSERT(b.encode_size(16) == 32);
  CPPUNIT_ASSERT_THROW(b.decode_size(15), ipfs_tiny::exception);
}

void
test_base16::t1()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t msg_len = 1024;
  ipfs_tiny::multiformats::base16<etl::ivector<uint8_t>, etl::istring> b;
  etl::vector<uint8_t, msg_len>                                        v0;
  etl::vector<uint8_t, msg_len>                                        v1;
  etl::string<msg_len * 2>                                             res;

  for (size_t i = 0; i < msg_len; i++) {
    v0.push_back(uni(mt));
  }

  b.encode(res, v0);
  b.decode(v1, res);
  CPPUNIT_ASSERT(etl::equal(v0.cbegin(), v0.cend(), v1.cbegin()));
}

void
test_base16::t2()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t msg_len = 1024;
  ipfs_tiny::multiformats::base16<etl::ivector<uint8_t>, etl::istring> b;
  etl::vector<uint8_t, msg_len>                                        v0;
  etl::vector<uint8_t, msg_len>                                        v1;
  etl::string<msg_len * 2>                                             res;

  for (size_t i = 0; i < msg_len; i++) {
    v0.push_back(uni(mt));
  }

  b.encode(res, v0);
  b.decode(v1, res.cbegin(), res.cend());
  CPPUNIT_ASSERT(v1 == v0);
}

void
test_base16::t3()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t msg_len = 1024;
  ipfs_tiny::multiformats::base16<etl::ivector<uint8_t>, etl::istring> b;
  etl::vector<uint8_t, msg_len>                                        v0;
  etl::vector<uint8_t, msg_len>                                        v1;
  etl::vector<uint8_t, msg_len>                                        v2;
  etl::vector<uint8_t, msg_len * 2>                                    res;
  etl::string<msg_len * 2>                                             str_res;

  for (size_t i = 0; i < msg_len; i++) {
    v0.push_back(uni(mt));
  }

  b.encode(res, v0);
  b.encode(str_res, v0);
  b.decode(v1, res.cbegin(), res.cend());
  CPPUNIT_ASSERT(v1 == v0);

  b.decode(v2, str_res.cbegin(), str_res.cend());
  CPPUNIT_ASSERT(v1 == v2);
}

void
test_base16::dec_malicious()
{
  etl::vector<uint8_t, 10> small_enc_vec;
  for (int i = 0; i < 10; i++) {
    small_enc_vec.push_back(i);
  }

  etl::vector<uint8_t, 30> medium_enc_vec;
  for (int i = 0; i < 30; i++) {
    medium_enc_vec.push_back(i);
  }

  etl::vector<uint8_t, 100> large_enc_vec;
  for (int i = 0; i < 100; i++) {
    large_enc_vec.push_back(i);
  }

  etl::vector<uint8_t, 10>  small_dec;
  etl::vector<uint8_t, 30>  medium_dec;
  etl::vector<uint8_t, 100> large_dec;

  ipfs_tiny::multiformats::base16<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
      b;

  CPPUNIT_ASSERT_THROW(b.decode(small_dec, small_enc_vec),
                       ipfs_tiny::multiformats::base16_decode_exception);
  CPPUNIT_ASSERT_THROW(b.decode(small_dec, small_enc_vec),
                       ipfs_tiny::multiformats::base16_decode_exception);
  CPPUNIT_ASSERT_THROW(b.decode(large_dec, large_enc_vec),
                       ipfs_tiny::multiformats::base16_decode_exception);
}