/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_CORE_HPP_
#define INCLUDE_IPFS_TINY_CORE_HPP_

#include "include/ipfs-tiny/interfaces/storage.hpp"

namespace ipfs_tiny
{

class core
{
  static storage &tinyStorage;
}

} // namespace ipfs_tiny

#endif /* INCLUDE_IPFS_TINY_CORE_HPP_ */
