/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_MULTIFORMATS_VARINT_HPP_
#define INCLUDE_IPFS_TINY_MULTIFORMATS_VARINT_HPP_

#include "etl/vector.h"

#include <cstdint>
#include <type_traits>

namespace ipfs_tiny::multiformats
{

class varint_invalid : public exception
{
public:
  varint_invalid(const char *file, size_t line)
      : exception("varint: invalid varint", file, line)
  {
  }
};

/**
 * @ingroup multiformats
 *
 * Multiformat compatible unsigned varint (VARiable INTeger) format encoding
 * and decoding
 */
class varint
{
public:
  /**
   * Maximum allowed varint length in bytes. Allows for
   * a maximum integer of 2^63
   */
  constexpr static size_t max_len = 9;

  template <typename T, typename IntT = uint64_t>
  static T &
  encode(T &                                                            res,
         typename std::enable_if_t<std::is_integral<IntT>::value, IntT> len)
  {
    if (len == 0) {
      res.push_back(0);
      return res;
    }
    while (len) {
      uint8_t x = len & 0x7F;
      len       = len >> 7;
      if (len) {
        x |= (1 << 7);
      }
      res.push_back(x);
    }
    return res;
  }

  /**
   * Decodes a varint byte sequence into the corresponding integer
   * @tparam InputIterator input iterator of byte elements
   * @param res the decoded integer result
   * @param first the first input element
   * @param last the last input element
   * @return the next input element, after the last parsed varint element
   */
  template <typename InputIterator>
  static InputIterator
  decode(uint64_t &res, InputIterator first, InputIterator last)
  {
    const size_t len = std::min<size_t>(last - first, max_len);
    uint64_t     r   = 0;
    for (size_t i = 0; i < len; i++) {
      uint8_t x = *first;
      first++;
      r |= (((uint64_t)(x & 0x7F)) << (i * 7));
      if ((x & (1 << 7)) == 0) {
        res = r;
        return first;
      }
    }
    throw varint_invalid(__FILE__, __LINE__);
  }
};

} // namespace ipfs_tiny::multiformats

#endif /* INCLUDE_IPFS_TINY_MULTIFORMATS_VARINT_HPP_ */
