/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_CRYPTO_BASE32_HPP
#define INCLUDE_IPFS_TINY_CRYPTO_BASE32_HPP

#include "base.hpp"
#include "etl/format_spec.h"
#include <cstddef>
#include <etl/map.h>
#include <etl/string.h>
#include <etl/vector.h>

#include <iostream>

namespace ipfs_tiny::multiformats
{

class base32_decode_exception : public exception
{
public:
  base32_decode_exception(const char *file, size_t line)
      : exception("base32 decoding error", file, line)
  {
  }
};

enum class padding_rune : uint8_t
{
  NoPadding  = 0,
  StdPadding = '='
};

/**
 * base32 (multibase code: f) class.
 *
 * @tparam T1 input data datatype
 * @tparam T2 output data datatype
 */
template <typename T1, typename T2> class base32 : public base<T1, T2>
{
private:
  static constexpr char b32digits_ordered[] =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
  padding_rune pad_char = padding_rune::StdPadding;

  class b32digits_map
  {
  public:
    constexpr b32digits_map()
    {
      for (size_t i = 0; i < 32; i++) {
        map[b32digits_ordered[i]] = i;
      }
    }

    constexpr uint8_t
    at(uint8_t i)
    {
      return map.at(i);
    }

  private:
    etl::map<uint8_t, uint8_t, 32> map;
  };

public:
  base32() : base<T1, T2>(etl::make_string("base32"), 'b') {}

  void
  padding(padding_rune _pad_char) const
  {
    switch (_pad_char) {
    case padding_rune::NoPadding:
      pad_char = padding_rune::NoPadding;
      break;
    case padding_rune::StdPadding:
      pad_char = padding_rune::StdPadding;
      break;
    default:
      throw base32_decode_exception(__FILE__, __LINE__);
    }
  }

  etl::ivector<uint8_t> &
  encode(etl::ivector<uint8_t> &res, const etl::ivector<uint8_t> &data)
  {
    ssize_t i = res.size(), j = 0, zcount = 0;
    size_t  size;

    while (zcount < (ssize_t)data.size() && !data[zcount])
      ++zcount;

    size = base32::encode_size(data.size() - zcount);

    res.resize(res.size() + size);

    while (data.size() - j > 4) {
      res[7 + i] = b32digits_ordered[data[4 + j] & 0x1F];
      res[6 + i] =
          b32digits_ordered[(data[4 + j] >> 5) | (data[3 + j] << 3) & 0x1F];
      res[5 + i] = b32digits_ordered[(data[3 + j] >> 2) & 0x1F];
      res[4 + i] =
          b32digits_ordered[(data[3 + j] >> 7) | (data[2 + j] << 1) & 0x1F];
      res[3 + i] =
          b32digits_ordered[(data[2 + j] >> 4) | (data[1 + j] << 4) & 0x1F];
      res[2 + i] = b32digits_ordered[(data[1 + j] >> 1) & 0x1F];
      res[1 + i] =
          b32digits_ordered[((data[1 + j] >> 6) | (data[j] << 2)) & 0x1F];
      res[i] = b32digits_ordered[data[j] >> 3];
      i += 8;
      j += 5;
    }

    uint8_t carry = 0U;
    switch (data.size() - j) {
    case 4:
      res[6 + i] = b32digits_ordered[(data[3 + j] << 3) & 0x1F];
      res[5 + i] = b32digits_ordered[(data[3 + j] >> 2) & 0x1F];
      carry      = data[3 + j] >> 7;
    case 3:
      res[4 + i] = b32digits_ordered[carry | (data[2 + j] << 1) & 0x1F];
      carry      = (data[2 + j] >> 4) & 0x1F;
    case 2:
      res[3 + i] = b32digits_ordered[carry | (data[1 + j] << 4) & 0x1F];
      res[2 + i] = b32digits_ordered[(data[1 + j] >> 1) & 0x1F];
      carry      = (data[1 + j] >> 6) & 0x1F;
    case 1:
      res[1 + i] = b32digits_ordered[carry | (data[j] << 2) & 0x1F];
      res[i]     = b32digits_ordered[data[j] >> 3];
      break;
    default:
      return res;
    }

    if (pad_char != padding_rune::NoPadding) {
      res[7 + i] = static_cast<uint8_t>(pad_char);
      if (data.size() - j < 4) {
        res[6 + i] = static_cast<uint8_t>(pad_char);
        res[5 + i] = static_cast<uint8_t>(pad_char);
        if (data.size() - j < 3) {
          res[4 + i] = static_cast<uint8_t>(pad_char);
          if (data.size() - j < 2) {
            res[3 + i] = static_cast<uint8_t>(pad_char);
            res[2 + i] = static_cast<uint8_t>(pad_char);
          }
        }
      }
    }

    return res;
  }

  etl::ivector<uint8_t> &
  encode(etl::ivector<uint8_t> &               res,
         etl::ivector<uint8_t>::const_iterator begin,
         etl::ivector<uint8_t>::const_iterator end)
  {
    ssize_t i = res.size(), j = 0, zcount = 0;
    size_t  datasz = end - begin, size;

    while (zcount < (ssize_t)datasz && !(*(begin + zcount)))
      ++zcount;

    size = base32::encode_size(datasz - zcount);

    res.resize(res.size() + size);

    while (datasz - j > 4) {
      res[7 + i] = b32digits_ordered[*(begin + 4 + j) & 0x1F];
      res[6 + i] = b32digits_ordered[(*(begin + 4 + j) >> 5) |
                                     (*(begin + 3 + j) << 3) & 0x1F];
      res[5 + i] = b32digits_ordered[(*(begin + 3 + j) >> 2) & 0x1F];
      res[4 + i] = b32digits_ordered[(*(begin + 3 + j) >> 7) |
                                     (*(begin + 2 + j) << 1) & 0x1F];
      res[3 + i] = b32digits_ordered[(*(begin + 2 + j) >> 4) |
                                     (*(begin + 1 + j) << 4) & 0x1F];
      res[2 + i] = b32digits_ordered[(*(begin + 1 + j) >> 1) & 0x1F];
      res[1 + i] =
          b32digits_ordered[((*(begin + 1 + j) >> 6) | (*(begin + j) << 2)) &
                            0x1F];
      res[i] = b32digits_ordered[*(begin + j) >> 3];
      i += 8;
      j += 5;
    }

    uint8_t carry = 0U;
    switch (datasz - j) {
    case 4:
      res[6 + i] = b32digits_ordered[(*(begin + 3 + j) << 3) & 0x1F];
      res[5 + i] = b32digits_ordered[(*(begin + 3 + j) >> 2) & 0x1F];
      carry      = *(begin + 3 + j) >> 7;
    case 3:
      res[4 + i] = b32digits_ordered[carry | (*(begin + 2 + j) << 1) & 0x1F];
      carry      = (*(begin + 2 + j) >> 4) & 0x1F;
    case 2:
      res[3 + i] = b32digits_ordered[carry | (*(begin + 1 + j) << 4) & 0x1F];
      res[2 + i] = b32digits_ordered[(*(begin + 1 + j) >> 1) & 0x1F];
      carry      = (*(begin + 1 + j) >> 6) & 0x1F;
    case 1:
      res[1 + i] = b32digits_ordered[carry | (*(begin + j) << 2) & 0x1F];
      res[i]     = b32digits_ordered[*(begin + j) >> 3];
      break;
    default:
      return res;
    }

    if (pad_char != padding_rune::NoPadding) {
      res[7 + i] = static_cast<uint8_t>(pad_char);
      if (datasz - j < 4) {
        res[6 + i] = static_cast<uint8_t>(pad_char);
        res[5 + i] = static_cast<uint8_t>(pad_char);
        if (datasz - j < 3) {
          res[4 + i] = static_cast<uint8_t>(pad_char);
          if (datasz - j < 2) {
            res[3 + i] = static_cast<uint8_t>(pad_char);
            res[2 + i] = static_cast<uint8_t>(pad_char);
          }
        }
      }
    }

    return res;
  }

  etl::istring &
  encode(etl::istring &res, const etl::ivector<uint8_t> &data)
  {
    ssize_t i = res.size(), j = 0, zcount = 0;
    size_t  size;

    while (zcount < (ssize_t)data.size() && !data[zcount])
      ++zcount;

    size = base32::encode_size(data.size() - zcount);

    res.resize(res.size() + size);

    while (data.size() - j > 4) {
      res[7 + i] = b32digits_ordered[data[4 + j] & 0x1F];
      res[6 + i] =
          b32digits_ordered[(data[4 + j] >> 5) | (data[3 + j] << 3) & 0x1F];
      res[5 + i] = b32digits_ordered[(data[3 + j] >> 2) & 0x1F];
      res[4 + i] =
          b32digits_ordered[(data[3 + j] >> 7) | (data[2 + j] << 1) & 0x1F];
      res[3 + i] =
          b32digits_ordered[(data[2 + j] >> 4) | (data[1 + j] << 4) & 0x1F];
      res[2 + i] = b32digits_ordered[(data[1 + j] >> 1) & 0x1F];
      res[1 + i] =
          b32digits_ordered[((data[1 + j] >> 6) | (data[j] << 2)) & 0x1F];
      res[i] = b32digits_ordered[data[j] >> 3];
      i += 8;
      j += 5;
    }

    uint8_t carry = 0U;
    switch (data.size() - j) {
    case 4:
      res[6 + i] = b32digits_ordered[(data[3 + j] << 3) & 0x1F];
      res[5 + i] = b32digits_ordered[(data[3 + j] >> 2) & 0x1F];
      carry      = data[3 + j] >> 7;
    case 3:
      res[4 + i] = b32digits_ordered[carry | (data[2 + j] << 1) & 0x1F];
      carry      = (data[2 + j] >> 4) & 0x1F;
    case 2:
      res[3 + i] = b32digits_ordered[carry | (data[1 + j] << 4) & 0x1F];
      res[2 + i] = b32digits_ordered[(data[1 + j] >> 1) & 0x1F];
      carry      = (data[1 + j] >> 6) & 0x1F;
    case 1:
      res[1 + i] = b32digits_ordered[carry | (data[j] << 2) & 0x1F];
      res[i]     = b32digits_ordered[data[j] >> 3];
      break;
    default:
      return res;
    }

    if (pad_char != padding_rune::NoPadding) {
      res[7 + i] = static_cast<uint8_t>(pad_char);
      if (data.size() - j < 4) {
        res[6 + i] = static_cast<uint8_t>(pad_char);
        res[5 + i] = static_cast<uint8_t>(pad_char);
        if (data.size() - j < 3) {
          res[4 + i] = static_cast<uint8_t>(pad_char);
          if (data.size() - j < 2) {
            res[3 + i] = static_cast<uint8_t>(pad_char);
            res[2 + i] = static_cast<uint8_t>(pad_char);
          }
        }
      }
    }

    return res;
  }

  etl::istring &
  encode(etl::istring &res, etl::ivector<uint8_t>::const_iterator begin,
         etl::ivector<uint8_t>::const_iterator end)
  {
    ssize_t i = res.size(), j = 0, zcount = 0;
    size_t  datasz = end - begin, size;

    while (zcount < (ssize_t)datasz && !(*(begin + zcount)))
      ++zcount;

    size = base32::encode_size(datasz - zcount);

    res.resize(res.size() + size);

    while (datasz - j > 4) {
      res[7 + i] = b32digits_ordered[*(begin + 4 + j) & 0x1F];
      res[6 + i] = b32digits_ordered[(*(begin + 4 + j) >> 5) |
                                     (*(begin + 3 + j) << 3) & 0x1F];
      res[5 + i] = b32digits_ordered[(*(begin + 3 + j) >> 2) & 0x1F];
      res[4 + i] = b32digits_ordered[(*(begin + 3 + j) >> 7) |
                                     (*(begin + 2 + j) << 1) & 0x1F];
      res[3 + i] = b32digits_ordered[(*(begin + 2 + j) >> 4) |
                                     (*(begin + 1 + j) << 4) & 0x1F];
      res[2 + i] = b32digits_ordered[(*(begin + 1 + j) >> 1) & 0x1F];
      res[1 + i] =
          b32digits_ordered[((*(begin + 1 + j) >> 6) | (*(begin + j) << 2)) &
                            0x1F];
      res[i] = b32digits_ordered[*(begin + j) >> 3];
      i += 8;
      j += 5;
    }

    uint8_t carry = 0U;
    switch (datasz - j) {
    case 4:
      res[6 + i] = b32digits_ordered[(*(begin + 3 + j) << 3) & 0x1F];
      res[5 + i] = b32digits_ordered[(*(begin + 3 + j) >> 2) & 0x1F];
      carry      = *(begin + 3 + j) >> 7;
    case 3:
      res[4 + i] = b32digits_ordered[carry | (*(begin + 2 + j) << 1) & 0x1F];
      carry      = (*(begin + 2 + j) >> 4) & 0x1F;
    case 2:
      res[3 + i] = b32digits_ordered[carry | (*(begin + 1 + j) << 4) & 0x1F];
      res[2 + i] = b32digits_ordered[(*(begin + 1 + j) >> 1) & 0x1F];
      carry      = (*(begin + 1 + j) >> 6) & 0x1F;
    case 1:
      res[1 + i] = b32digits_ordered[carry | (*(begin + j) << 2) & 0x1F];
      res[i]     = b32digits_ordered[*(begin + j) >> 3];
      break;
    default:
      return res;
    }

    if (pad_char != padding_rune::NoPadding) {
      res[7 + i] = static_cast<uint8_t>(pad_char);
      if (datasz - j < 4) {
        res[6 + i] = static_cast<uint8_t>(pad_char);
        res[5 + i] = static_cast<uint8_t>(pad_char);
        if (datasz - j < 3) {
          res[4 + i] = static_cast<uint8_t>(pad_char);
          if (datasz - j < 2) {
            res[3 + i] = static_cast<uint8_t>(pad_char);
            res[2 + i] = static_cast<uint8_t>(pad_char);
          }
        }
      }
    }

    return res;
  }

  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &res, const etl::ivector<uint8_t> &data)
  {
    bool   end = false;
    size_t i = res.size(), len = 0;

    res.resize(res.size() + base32::decode_size(data.size()));

    while (len < data.size() && !end) {
      uint8_t dbuf[8] = {0};
      size_t  dlen    = 8;

      for (size_t j = 0; j < 8; ++j) {
        if (len == data.size()) {
          if (pad_char != padding_rune::NoPadding) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }
          dlen = j;
          break;
        }

        uint8_t in = data[len++];
        if (in == static_cast<uint8_t>(pad_char) && j >= 2 &&
            (data.size() - len) < 8) {
          if (pad_char == padding_rune::NoPadding) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }

          if (data.size() - len + j < 7) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }

          for (size_t k = 0; k < 7 - j; ++k) {
            if ((data.size() - len) > k &&
                data[len + k] != static_cast<uint8_t>(pad_char)) {
              throw base32_decode_exception(__FILE__, __LINE__);
            }
          }

          dlen = j;
          end  = true;

          if (dlen == 1 || dlen == 3 || dlen == 6) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }
          break;
        }
        dbuf[j] = b32digits_map().at(in);
        if (dbuf[j] == 0xFF) {
          throw base32_decode_exception(__FILE__, __LINE__);
        }
      }

      switch (dlen) {
      case 8:
        res[4 + i] = dbuf[6] << 5 | dbuf[7];
      case 7:
        res[3 + i] = dbuf[4] << 7 | dbuf[5] << 2 | dbuf[6] >> 3;
      case 5:
        res[2 + i] = dbuf[3] << 4 | dbuf[4] >> 1;
      case 4:
        res[1 + i] = dbuf[1] << 6 | dbuf[2] << 1 | dbuf[3] >> 4;
      case 2:
        res[i] = dbuf[0] << 3 | dbuf[1] >> 2;
        break;
      default:
        throw base32_decode_exception(__FILE__, __LINE__);
      }
      i += 5;
    }

    return res;
  }

  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &res, const etl::istring &data)
  {
    bool   end = false;
    size_t i = res.size(), len = 0;

    res.resize(res.size() + base32::decode_size(data.size()));

    while (len < data.size() && !end) {
      uint8_t dbuf[8] = {0};
      size_t  dlen    = 8;

      for (size_t j = 0; j < 8; ++j) {
        if (len == data.size()) {
          if (pad_char != padding_rune::NoPadding) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }
          dlen = j;
          break;
        }

        uint8_t in = data[len++];
        if (in == static_cast<uint8_t>(pad_char) && j >= 2 &&
            (data.size() - len) < 8) {
          if (pad_char == padding_rune::NoPadding) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }

          if (data.size() - len + j < 7) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }

          for (size_t k = 0; k < 7 - j; ++k) {
            if ((data.size() - len) > k &&
                data[len + k] != static_cast<uint8_t>(pad_char)) {
              throw base32_decode_exception(__FILE__, __LINE__);
            }
          }

          dlen = j;
          end  = true;

          if (dlen == 1 || dlen == 3 || dlen == 6) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }
          break;
        }
        dbuf[j] = b32digits_map().at(in);
        if (dbuf[j] == 0xFF) {
          throw base32_decode_exception(__FILE__, __LINE__);
        }
      }

      switch (dlen) {
      case 8:
        res[4 + i] = dbuf[6] << 5 | dbuf[7];
      case 7:
        res[3 + i] = dbuf[4] << 7 | dbuf[5] << 2 | dbuf[6] >> 3;
      case 5:
        res[2 + i] = dbuf[3] << 4 | dbuf[4] >> 1;
      case 4:
        res[1 + i] = dbuf[1] << 6 | dbuf[2] << 1 | dbuf[3] >> 4;
      case 2:
        res[i] = dbuf[0] << 3 | dbuf[1] >> 2;
        break;
      default:
        throw base32_decode_exception(__FILE__, __LINE__);
      }
      i += 5;
    }

    return res;
  }

  /**
   * Decodes a range of data in the range of [\p begin, \p end)
   * @param res the structure to append the result
   * @param begin iterator to the first input data
   * @param end iterator to the last input data
   * @return reference to \p res
   */
  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &res, etl::istring::const_iterator begin,
         etl::istring::const_iterator end)
  {
    bool   finished = false;
    size_t i = res.size(), len = 0, datasz = end - begin;

    res.resize(res.size() + base32::decode_size(datasz));

    while (len < datasz && !finished) {
      uint8_t dbuf[8] = {0};
      size_t  dlen    = 8;

      for (size_t j = 0; j < 8; ++j) {
        if (len == datasz) {
          if (pad_char != padding_rune::NoPadding) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }
          dlen = j;
          break;
        }

        uint8_t in = *(begin + len++);
        if (in == static_cast<uint8_t>(pad_char) && j >= 2 &&
            (datasz - len) < 8) {
          if (pad_char == padding_rune::NoPadding) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }

          if (datasz - len + j < 7) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }

          for (size_t k = 0; k < 7 - j; ++k) {
            if ((datasz - len) > k &&
                *(begin + len + k) != static_cast<uint8_t>(pad_char)) {
              throw base32_decode_exception(__FILE__, __LINE__);
            }
          }

          dlen     = j;
          finished = true;

          if (dlen == 1 || dlen == 3 || dlen == 6) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }
          break;
        }
        dbuf[j] = b32digits_map().at(in);
        if (dbuf[j] == 0xFF) {
          throw base32_decode_exception(__FILE__, __LINE__);
        }
      }

      switch (dlen) {
      case 8:
        res[4 + i] = dbuf[6] << 5 | dbuf[7];
      case 7:
        res[3 + i] = dbuf[4] << 7 | dbuf[5] << 2 | dbuf[6] >> 3;
      case 5:
        res[2 + i] = dbuf[3] << 4 | dbuf[4] >> 1;
      case 4:
        res[1 + i] = dbuf[1] << 6 | dbuf[2] << 1 | dbuf[3] >> 4;
      case 2:
        res[i] = dbuf[0] << 3 | dbuf[1] >> 2;
        break;
      default:
        throw base32_decode_exception(__FILE__, __LINE__);
      }
      i += 5;
    }

    return res;
  }

  /**
   * Decodes a range of data in the range of [\p begin, \p end)
   * @param res the structure to append the result
   * @param begin iterator to the first input data
   * @param end iterator to the last input data
   * @return reference to \p res
   */
  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &               res,
         etl::ivector<uint8_t>::const_iterator begin,
         etl::ivector<uint8_t>::const_iterator end)
  {
    bool   finished = false;
    size_t i = res.size(), len = 0, datasz = end - begin;

    res.resize(res.size() + base32::decode_size(datasz));

    while (len < datasz && !finished) {
      uint8_t dbuf[8] = {0};
      size_t  dlen    = 8;

      for (size_t j = 0; j < 8; ++j) {
        if (len == datasz) {
          if (pad_char != padding_rune::NoPadding) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }
          dlen = j;
          break;
        }

        uint8_t in = *(begin + len++);
        if (in == static_cast<uint8_t>(pad_char) && j >= 2 &&
            (datasz - len) < 8) {
          if (pad_char == padding_rune::NoPadding) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }

          if (datasz - len + j < 7) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }

          for (size_t k = 0; k < 7 - j; ++k) {
            if ((datasz - len) > k &&
                *(begin + len + k) != static_cast<uint8_t>(pad_char)) {
              throw base32_decode_exception(__FILE__, __LINE__);
            }
          }

          dlen     = j;
          finished = true;

          if (dlen == 1 || dlen == 3 || dlen == 6) {
            throw base32_decode_exception(__FILE__, __LINE__);
          }
          break;
        }
        dbuf[j] = b32digits_map().at(in);
        if (dbuf[j] == 0xFF) {
          throw base32_decode_exception(__FILE__, __LINE__);
        }
      }

      switch (dlen) {
      case 8:
        res[4 + i] = dbuf[6] << 5 | dbuf[7];
      case 7:
        res[3 + i] = dbuf[4] << 7 | dbuf[5] << 2 | dbuf[6] >> 3;
      case 5:
        res[2 + i] = dbuf[3] << 4 | dbuf[4] >> 1;
      case 4:
        res[1 + i] = dbuf[1] << 6 | dbuf[2] << 1 | dbuf[3] >> 4;
      case 2:
        res[i] = dbuf[0] << 3 | dbuf[1] >> 2;
        break;
      default:
        throw base32_decode_exception(__FILE__, __LINE__);
      }
      i += 5;
    }

    return res;
  }

  size_t
  encode_size(size_t input_size) const
  {
    return (input_size + 4) / 5 * 8;
  }

  size_t
  decode_size(size_t input_size) const
  {
    return input_size / 8 * 5;
  }
};

} // namespace ipfs_tiny::multiformats

#endif /* INCLUDE_IPFS_TINY_CRYPTO_BASE32_HPP */
