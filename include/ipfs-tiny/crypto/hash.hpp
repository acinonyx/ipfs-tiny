/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_CRYPTO_HASH_HPP_
#define INCLUDE_IPFS_TINY_CRYPTO_HASH_HPP_

#include "etl/string.h"
#include "etl/vector.h"
#include <cstddef>

namespace ipfs_tiny::crypto
{

/**
 *
 *
 * @brief Hash base class
 */

/**
 * @ingroup crypto
 *
 * The hash() class is the base class of all supported multihash hash functions.
 * It provides a generic API that is based on containers. Specialization should
 * be supported by the different hash function implementations.
 *
 * @tparam T a container that supports appending operations and sequential
 * read access. In most cases this should be etl::vector<uint8_t>.
 *
 */
template <typename T> class hash
{
public:
  /**
   *
   * @param name the name of the hash function as specified by the multihash
   * specification
   * @param fn the function code as specified by the multihash specification
   * @param size the size of the resulting digest in bits
   */
  hash(const etl::istring &name, size_t fn, size_t size)
      : m_name(name), m_fn(fn), m_size(size)
  {
  }

  virtual ~hash() {}

  /**
   *
   * @return the size of the resulting digest in bits
   */
  size_t
  size() const
  {
    return m_size;
  }

  /**
   *
   * @return the multihash function code as specified by the
   * https://github.com/multiformats/multicodec/blob/master/table.csv
   */
  size_t
  fn() const
  {
    return m_fn;
  }

  /**
   *
   * @return the name of the hash function as specified by the
   * https://github.com/multiformats/multicodec/blob/master/table.csv
   */
  const etl::istring &
  name() const
  {
    return m_name;
  }

  /**
   * Computes the digest of the input data
   * @param res vector to store the resulting digest
   * @param data input data
   * @return the reference to res
   */
  virtual T &
  digest(T &res, const T &data) = 0;

  /**
   * Computes the digest of the data processed so far using the update() method
   * @param res vector to store the resulting digest
   * @return the reference to res
   */
  virtual T &
  digest(T &res) = 0;

  virtual T &
  digest(T &res, typename T::const_iterator begin,
         typename T::const_iterator end) = 0;

  /**
   * Updates the internal computation with new data
   * @param data input data
   */
  virtual void
  update(const T &data) = 0;

  virtual void
  update(typename T::const_iterator begin, typename T::const_iterator end) = 0;

  /**
   * Resets the internal state to the initial state
   */
  virtual void
  reset() = 0;

private:
  const etl::string<32> m_name;
  const size_t          m_fn;
  const size_t          m_size;
};

} // namespace ipfs_tiny::crypto

#endif /* INCLUDE_IPFS_TINY_CRYPTO_HASH_HPP_ */
