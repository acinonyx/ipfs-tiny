/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_STORAGE_HPP_
#define INCLUDE_IPFS_TINY_STORAGE_HPP_

#include <etl/string.h>
#include <etl/vector.h>

namespace ipfs_tiny
{

class storage
{
public:
  /* Stores data with name as key */
  virtual bool
  store(const etl::istring &name, const etl::ivector<uint8_t> &data)
  {
    return true;
  };
  /* Read data stored under specific name */
  virtual bool
  read(const etl::istring &name, etl::ivector<uint8_t> &res)
  {
    return true;
  };
  /* Delete data stored under specific name */
  virtual bool
  erase(const etl::istring &name)
  {
    return true;
  };
  /* Return available storage space */
  virtual size_t
  available_space()
  {
    return 0;
  };
  /* Attempt a garbage collection and return freed up space */
  virtual size_t
  garbage_collection()
  {
    return 0;
  };
};

} // namespace ipfs_tiny

#endif /* INCLUDE_IPFS_TINY_STORAGE_HPP_ */
