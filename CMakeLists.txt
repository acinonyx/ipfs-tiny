# SPDX-License-Identifier: GPL-3.0-or-later

cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(ipfs-tiny VERSION 0.0)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

###############################################################################
# CMake custom modules
###############################################################################
list(INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/nanopb/extra)

###############################################################################
# Testing support
###############################################################################
option(ENABLE_TESTING "Enable Unit testing" ON)


###############################################################################
# Build requirements
###############################################################################
find_package(Nanopb REQUIRED)

###############################################################################
# Code coverage support
###############################################################################
option(ENABLE_COVERAGE
    "Enable a code coverage report generation" OFF
)

if(ENABLE_COVERAGE)
  include(CodeCoverage)
  APPEND_COVERAGE_COMPILER_FLAGS()
endif(ENABLE_COVERAGE)

if(ENABLE_TESTING)
    include (CTest)
    enable_testing()
    add_subdirectory(test)
endif()

add_subdirectory(src)